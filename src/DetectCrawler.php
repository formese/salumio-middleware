<?php

namespace SalumIo\Middleware;

use Jaybizzle\CrawlerDetect\CrawlerDetect;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class DetectCrawler
{
    /**
     * @var CrawlerDetect
     */
    protected $crawlerDetect;

    /**
     * @param CrawlerDetect $crawlerDetect
     */
    public function __construct(CrawlerDetect $crawlerDetect)
    {
        $this->crawlerDetect = $crawlerDetect;
    }

    /**
     * Execute the middleware.
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     * @param callable               $next
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
    {
        $isCrawler = $this->crawlerDetect->isCrawler($request->getHeaderLine('User-Agent'));
        $request = $request->withAttribute('is_crawler', $isCrawler);
        if ($isCrawler) {
            $request->withAttribute('crawler', $this->crawlerDetect->getMatches());
        }

        return $next($request, $response);
    }
}
